﻿using System;
using System.Collections.Generic;
using System.Text;

namespace todo_domain_entities
{
    public class TodoList
    {
        public int ID { get; set; }
        public string ListName { get; set; }
        public bool IsHidden { get; set; } = false;
        public string Description { get; set; }
        public List<Todo> Todos { get; set; } = new List<Todo>();
    }
}
