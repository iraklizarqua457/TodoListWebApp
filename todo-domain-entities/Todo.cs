﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities
{
    public class Todo : IValidatableObject
    {
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Display(Name = "Entry Title")]
        [Required(ErrorMessage = "Entry Title can't be empty")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Entry title's length can't be less than 3 or more than 25 characters")]
        public string Title { get; set; }
        [Display(Name = "Entry Description")]
        [Required(ErrorMessage = "Entry Description can't be empty")]
        [StringLength(255, MinimumLength = 30, ErrorMessage = "Entry Description's length can't be less than 30 or more than 255 characters")]
        public string Description { get; set; }
        [Display(Name = "Due Date and Time")]
        [Required(ErrorMessage = "Due Date and Time is Required")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date")]
        public DateTime DueDate { get; set; }
        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; } = DateTime.Now;
        [Display(Name = "Entry Status")]
        public string Status { get; set; }
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();

            if (DateTime.Compare(DateTime.Now, DueDate) > 0)
            {
                ValidationResult mss = new ValidationResult("Due date/time must be greater than or equal to the current date/time");
                res.Add(mss);
            }

                return res;
        }

    }
}
