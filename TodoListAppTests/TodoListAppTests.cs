using CapstoneProject.Data;
using CapstoneProject.Repository;
using NUnit.Framework;
using System;
using System.Globalization;
using System.Linq;
using todo_domain_entities;

namespace TodoListAppTests
{
    public class Tests
    {
        private readonly TodoListRepository repo = new TodoListRepository(new ApplicationDbContext());

        [TestCase("List1", "List2")]
        public void AddTodoList_Test(string listName, string listName1)
        {
            var list1 = new TodoList
            {
                ListName = listName,
            };

            var list2 = new TodoList
            {
                ListName = listName1,
            };

            repo.DeleteAllRows();
            repo.AddTodoList(list1);
            repo.AddTodoList(list2);

            Assert.True(repo.TodoLists.ToList().Exists(x => x.ListName == listName) || repo.TodoLists.ToList().Exists(x => x.ListName == listName1));

            this.repo.DeleteAllRows();

        }
        [Test]
        public void DeleteTodoLists_Test()
        {
            repo.DeleteAllRows();
            repo.AddTodoList(new TodoList { ListName = "Work" });
            repo.AddTodoList(new TodoList { ListName = "Home" });

            repo.AddTodoEntry(repo.TodoLists.First(x => x.ListName == "Work").ID,
                new Todo
                {
                    Title = "Tasks at work",
                    Description = "The tasks that I have to do at work",
                    CreationTime = DateTime.Now,
                    Status = "Not Started",
                    DueDate = DateTime.Now.AddHours(3),

                });
            repo.AddTodoEntry(repo.TodoLists.First(x => x.ListName == "Home").ID,
                new Todo
                {
                    Title = "Tasks at home",
                    Description = "The tasks that I have to do at home",
                    CreationTime = DateTime.Now,
                    Status = "Not Started",
                    DueDate = DateTime.Now.AddDays(2),
                });
            repo.DeleteTodoList(repo.TodoLists.FirstOrDefault(x => x.ListName == "Work").ID);
            Assert.False(repo.TodoLists.ToList().Exists(x => x.ListName == "Work"));
            repo.DeleteTodoList(repo.TodoLists.FirstOrDefault(x => x.ListName == "Home").ID);
            Assert.False(repo.TodoLists.ToList().Exists(x => x.ListName == "Home"));

        }
        [TestCase("HomeList", "First Task", "First task description")]
        public void DeleteTodo_test(string listName, string todoName, string description)
        {
            repo.DeleteAllRows();

            repo.AddTodoList(new TodoList { ListName = listName });

            repo.AddTodoEntry(
                this.repo.TodoLists.First(x => x.ListName == listName).ID,
                new Todo
                {
                    Title = todoName,
                    Description = description,
                    DueDate = DateTime.Now,
                });

            repo.DeleteTodoEntry(repo.TodoEntries.First(x => x.Title == todoName).ID);
            Assert.False(repo.GetList(repo.TodoLists.First(x => x.ListName == listName).ID).Todos.Exists(x => x.Title == todoName));
        }
        [TestCase("Home Tasks List", "Tasks Around the home", "the tasks that I have to do around the home", "06/06/2023","In Progress")]
        public void ChangeTodo_test(string listName,string todoName, string description, string dueDate, string status)
        {
            repo.DeleteAllRows();
            DateTime dt = DateTime.ParseExact(dueDate, "dd/mm/yyyy", CultureInfo.InvariantCulture);
            repo.AddTodoList(new TodoList { ListName = listName });
            repo.AddTodoEntry(
                repo.TodoLists.First(x => x.ListName == listName).ID,
                new Todo
                {
                    Title = "Title",
                    Status = "Completed",
                    Description = "Some description",
                    DueDate = DateTime.Now.AddDays(6),
                    CreationTime = DateTime.Now
                });
            repo.UpdateEntry(
                repo.TodoEntries.First(x => x.Title == "Title").ID,
                new Todo
                { 
                    Title = todoName,
                    Status = status,
                    Description = description,
                    DueDate = dt,
                });
            Assert.True(repo.TodoLists.First(x => x.ListName == listName).Todos.First().Title == todoName);
            Assert.True(repo.TodoLists.First(x => x.ListName == listName).Todos.First().Status == status);
            Assert.True(repo.TodoLists.First(x => x.ListName == listName).Todos.First().Description == description);
            Assert.True(repo.TodoLists.First(x => x.ListName == listName).Todos.First().DueDate == dt);
        }
        [TestCase("Home", "Home tasks")]
        public void ChangeList_Test(string listName1, string listDesc)
        {
            repo.DeleteAllRows();

            var listName = "List Name";

            repo.AddTodoList(
                new TodoList
                {
                    ListName = listName
                });

            repo.UpdateList(
                repo.TodoLists.First(x => x.ListName == "List Name").ID,
                new TodoList
                {
                    ListName = listName1,
                    IsHidden = true,
                    Description = listDesc
                    
                });

            Assert.True(repo.TodoLists.First().ListName == listName1);
            Assert.True(repo.TodoLists.First().Description == listDesc);
            Assert.True(repo.TodoLists.First().IsHidden);

        }
    }
}