﻿using CapstoneProject.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace CapstoneProject.Repository
{
    public class TodoListRepository : ITodoListRepository
    {
        public readonly ApplicationDbContext _context;

        public TodoListRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<TodoList> TodoLists => _context.TodoLists.Include(x => x.Todos);

        public IQueryable<Todo> TodoEntries => _context.Todos;

        public void AddTodoEntry(int listID, Todo todoEntry)
        {
            var todoList = _context.TodoLists.Include(inc => inc.Todos).Single(x => x.ID == listID);
            todoList.Todos.Add(todoEntry);
            _context.SaveChanges();
        }

        public void AddTodoList(TodoList todoList)
        {
            _context.TodoLists.Add(todoList);
            _context.SaveChanges();
        }

        public void DeleteAllRows()
        {
            foreach (Todo todoEntry in _context.Todos)
            {
                _context.Todos.Remove(todoEntry);
            }

            foreach (TodoList todoList in _context.TodoLists)
            {
                _context.TodoLists.Remove(todoList);
            }

            _context.SaveChanges();
        }
        public void DeleteTodoEntry(int id)
        {
            var todoEntry = _context.Todos.First(x => x.ID == id);
            _context.Todos.Remove(todoEntry);
            _context.SaveChanges();
        }

        public void DeleteTodoList(int id)
        {
            var list = _context.TodoLists.Include(inc => inc.Todos).Single(x => x.ID == id);

            foreach (Todo entry in list.Todos)
            {
                _context.Todos.Remove(entry);
            }

            _context.TodoLists.Remove(list);
            _context.SaveChanges();

        }

        public Todo GetEntry(int id) => _context.Todos.First(x => x.ID == id);

        public TodoList GetList(int id) => _context.TodoLists.First(x => x.ID == id);

        public void UpdateEntry(int id, Todo todoEntry)
        {
            var todoentry = _context.Todos.Single(x => x.ID == id);

            todoentry.Title = todoEntry.Title;
            todoentry.Status = todoEntry.Status;
            todoentry.Description = todoEntry.Description;
            todoentry.DueDate = todoEntry.DueDate;

            _context.SaveChanges();
        }

        public void UpdateList(int id, TodoList todoList)
        {
            var todolist = _context.TodoLists.First(x => x.ID == id);

            todolist.ListName = todoList.ListName;
            todolist.IsHidden = todoList.IsHidden;
            todolist.Description = todoList.Description;

            _context.SaveChanges();
        }
    }
}
