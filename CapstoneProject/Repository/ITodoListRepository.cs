﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace CapstoneProject.Repository
{
    public interface ITodoListRepository
    {

        public interface ITodoListsRepository
        { 
            IQueryable<TodoList> TodoLists { get; }
            IQueryable<Todo> TodoEntries { get; }
            Todo GetEntry(int id);
            TodoList GetList(int id);
            void AddTodoList(TodoList todoList);
            void DeleteTodoList(int id);
            void AddTodoEntry(int listID, Todo todoEntry);
            void DeleteTodoEntry(int id);
            void DeleteAllRows();
            void UpdateEntry(int id, Todo todoEntry);
            void UpdateList(int id, TodoList todoList);
        }

    }
}
